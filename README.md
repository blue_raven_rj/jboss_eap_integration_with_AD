# JBoss Enterprise Application Platform 7 Integration with Microsoft Active Directory.


1. Create two Organizational Units called **Jboss Groups** and **Jboss Users**.
2. Inside **Jboss Groups** Organizational Unit, create a group called **Jboss_Admin**.
3. Inside **Jboss Users** Organizational Unit, create a user called **jboss_ldap_user** and then add it to **Jboss_Admin** group.
*  NOTE: **jboss_ldap_user** user will be used to authenticate with Microsoft Active Directory.

4. For better management of users/group you can create group as the following and then add user to the corresponding group:
* Jboss_Admin (note that **Jboss_Admin** group should only have **jboss_ldap_user** user as member)
* Jboss_Administrator
* Jboss_Auditor
* Jboss_Deployer
* Jboss_Maintainer
* Jboss_Monitor
* Jboss_Operator
* Jboss_SuperUser

**Check the pictures above for better understanding.**